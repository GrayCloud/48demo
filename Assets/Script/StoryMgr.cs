﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;
using System;

[System.Serializable]
public struct NPCInfo {
    public string name;
    public Sprite headImg;
    public WXScrollMgr scrolMgr;
}

public class StoryMgr : MonoBehaviour {
    const string configName = "config.json";
    const string beginStory = "dialog0";
    const int defaultDelayTime = 1000;
    private string userChooseFile;
    private string userName;
    private JSONArray chooseArray;
    private JSONClass totleConfigClass;
    private int delayTime = 0;
    private float passedTime = 0.0f;

    private JSONArray currentDialogs;
    private JSONClass waitingChoose;
    private string currentSender;
    private string currentDialogName;
    private int currentIndex;

    private bool updateFlag = false;
    private Dictionary<string, WXScrollMgr> npcInfoDict = new Dictionary<string, WXScrollMgr>();

    private static StoryMgr instance;
    public NPCInfo[] npcInfos;

    private void Awake()
    {
        for (int i = 0; i < npcInfos.Length; i++)
        {
            NPCInfo info = npcInfos[i];
            npcInfoDict.Add(info.name, info.scrolMgr);
            info.scrolMgr.InitSendHeadImg(info.headImg);
        }
        instance = this;
    }

    internal void MakeChoose(int v)
    {
        string nextChosse = waitingChoose.GetValueByKey("next")[v];
        string info = waitingChoose.GetValueByKey("info")[v];
        npcInfoDict[currentSender].AddSelfMsg(info);
        BeginNewStory(nextChosse);
    }

    internal void Quit()
    {
        string fullName = Path.Combine(Application.persistentDataPath, userChooseFile);
        if(File.Exists(fullName))
            File.Delete(fullName);
        UnityEditor.EditorApplication.isPlaying = false;
    }

    public static StoryMgr GetInstance() {
        return instance;
    }

    public string UserName{
        get { return userName; }
    }
    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (!updateFlag)
            return;
        if (delayTime > 0 && delayTime > passedTime) {
            passedTime += Time.deltaTime * 1000;
            return;
        }
        JSONClass dialogClass = currentDialogs[currentIndex] as JSONClass;
        if (delayTime > 0)
        {
            currentIndex++;
            handleOneNewMsg(npcInfoDict[currentSender], dialogClass);
            delayTime = 0;

        }
        else {
            delayTime = dialogClass.GetValueByKey("delay") == null ? defaultDelayTime : int.Parse(dialogClass.GetValueByKey("delay"));
            passedTime = 0.0f;
            npcInfoDict[currentSender].AddWaitingFlag();
        }
	}

    public void Init(string name) {
        userChooseFile = name + ".txt";
        userName = name;
        LoadHistoryChoose();
        LoadConfig();
        HandleHistoryMsgs();
    }


    void handleOneNewMsg(WXScrollMgr scrollMsg, JSONClass dialogClass) {
        string type = dialogClass.GetValueByKey("type");
        if (type == null || type == "text") {
            scrollMsg.AddTextMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), true);
            return;
        }
        if (type == "image")
        {
            scrollMsg.AddImgagMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), true);
        }
        else if (type == "choose") {
            JSONArray anniuArray = dialogClass.GetValueByKey("anniu") as JSONArray;
            scrollMsg.AddChooseMsg(anniuArray[0], anniuArray[1]);
            EndCurrentStory(currentDialogName);
        }
    }

    void HandleOneOldStory(string dialog, int nextIndex) {
        JSONClass oneStoryClass = totleConfigClass[dialog] as JSONClass;
        currentSender = oneStoryClass["sender"];
        if (!npcInfoDict.ContainsKey(currentSender))
            return;
        WXScrollMgr scrollMsg = npcInfoDict[currentSender];
        JSONArray dialogs = oneStoryClass["dialogArray"] as JSONArray;
        for (int i = 0; i < dialogs.Count; i++) {
            JSONClass dialogClass = dialogs[i] as JSONClass;
            string type = dialogClass.GetValueByKey("type");
            if (type == null || type == "text")
            {
                scrollMsg.AddTextMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), false);
                continue;
            }

            if (type == "image")
            {
                scrollMsg.AddImgagMsg(dialogClass.GetValueByKey("info"), dialogClass.GetValueByKey("time"), false);
            }
            else if (type == "choose")
            {
                if (nextIndex >= chooseArray.Count)
                {
                    JSONArray anniuArray = dialogClass.GetValueByKey("anniu") as JSONArray;
                    scrollMsg.AddChooseMsg(anniuArray[0], anniuArray[1]);
                    waitingChoose = dialogs[dialogs.Count - 1] as JSONClass;
                    updateFlag = false;
                }
                else {
                    JSONArray infoArray = dialogClass.GetValueByKey("info") as JSONArray;
                    JSONArray nextArray = dialogClass.GetValueByKey("next") as JSONArray;
                    string nextStory = chooseArray[nextIndex];
                    for (int j = 0; j < nextArray.Count; j++) {
                        if (nextArray[j] == nextStory) {
                            scrollMsg.AddSelfMsg(infoArray[i]);
                            break;
                        }
                    }
                }
            }
        }
    }

    void LoadHistoryChoose() {
        string fullName = Path.Combine(Application.persistentDataPath, userChooseFile);
        if (!File.Exists(fullName))
        {
            chooseArray = new JSONArray();
            return;
        }

        FileStream fs = File.OpenRead(fullName);
        using (StreamReader sr = new StreamReader(fs))
        {
            string s = sr.ReadToEnd();
            chooseArray = JSONClass.Parse(s) as JSONArray;
        }
        fs.Close();
    }

    void LoadConfig() {
        string fullName = Path.Combine(Application.streamingAssetsPath, configName);
        if (!File.Exists(fullName))
        {
            throw new Exception(fullName + " is missing!!!");
        }

        FileStream fs = File.OpenRead(fullName);
        using (StreamReader sr = new StreamReader(fs))
        {
            string s = sr.ReadToEnd();
            totleConfigClass = JSONClass.Parse(s) as JSONClass;
        }
        fs.Close();
    }

    void HandleHistoryMsgs() {
        if (chooseArray.Count > 0)
        {
            string dialog = "";
            for (int i = 0; i < chooseArray.Count; i++) {
                dialog = chooseArray[i];
                HandleOneOldStory(dialog, i + 1);
            }
        }
        else {
            BeginNewStory(beginStory);
        }
    }

    void BeginNewStory(string dialog) {
        currentDialogName = dialog;
        JSONClass currentDialogClass = totleConfigClass[dialog] as JSONClass;
        currentSender = currentDialogClass["sender"];
        currentDialogs = currentDialogClass["dialogArray"] as JSONArray;
        currentIndex = 0;
        updateFlag = true;
        AddChoose(dialog);
    }

    void EndCurrentStory(string dialog)
    {
        waitingChoose = currentDialogs[currentDialogs.Count - 1] as JSONClass;
        updateFlag = false;
    }

    void AddChoose(string dialog) {
        chooseArray[chooseArray.Count] = dialog;
        string choose = chooseArray.ToString();
        string path = Path.Combine(Application.persistentDataPath, userChooseFile);
        FileStream fs = File.Open(path, FileMode.Create);
        using (StreamWriter sw = new StreamWriter(fs))
        {
            sw.Write(choose);
        }
        fs.Close();
    }
}
