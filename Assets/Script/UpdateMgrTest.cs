﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMgrTest : MonoBehaviour {
    public AdapatableText text1;
    public AdapatableText text2;
    public AdapatableText text3;
    public WXScrollMgr scrollMsg;
    public StoryMgr storyMgr;

    // Use this for initialization
    void Start1 () {
        string test1 = "长度很短";
        string test2 = "长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长长度很长";
        string test3 = "引文数字afdgarga1111111111111asfdga222";
        text1.InputStr(test1);
        text2.InputStr(test2);
        text3.InputStr(test3);
    }

    void Start2()
    {
        string hisMsg = "长长度很长长度很长长度很长长度很长长度历史消息长长度很长长度很长长度很长长度很长长度";
        string[] listStr = new string[10];
        for (int i = 0; i < 10; i++) {
            listStr[i] = hisMsg + i;
        }
        scrollMsg.InitHistoryMsg(listStr);
    }

    private void Start()
    {
        storyMgr.Init("李四222");
    }

    public void addNewMsg() {
        string str = "新消息啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊";
        int rand = Random.Range(1, 10);
        scrollMsg.AddNewMsg(str + rand);
    }

    public void ChooseOne()
    {
        StoryMgr.GetInstance().MakeChoose(0);
    }

    public void ChooseSecond()
    {
        StoryMgr.GetInstance().MakeChoose(1);
    }

    public void Quit()
    {
        StoryMgr.GetInstance().Quit();
    }
}
