﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdapatableText : MonoBehaviour {
    public int testLeft;
    public int testRight;
    public int testTop;
    public int testBottom;
    public int maxCountPerRow;
    public int perRowHight;
    public int PerWordWidth;
    public Text inputText;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InputStr(string data) {
        int widthAdd = testLeft + testRight;
        int heightAdd = testTop + testBottom;
        RectTransform tran = this.GetComponent<RectTransform>();
        if (data.Length < maxCountPerRow)
        {
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(data.Length * PerWordWidth + widthAdd, perRowHight+ heightAdd);
        }
        else {
            int row = (data.Length - 1) / maxCountPerRow + 1;
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(maxCountPerRow * PerWordWidth + widthAdd, perRowHight*row + heightAdd);
        }
        inputText.text = data;
    }
}
