﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WXScrollMgr : MonoBehaviour, IEndDragHandler, IBeginDragHandler
{
    public Scrollbar scrollBar;
    public GameObject textPrefab;
    public GameObject contentParant;

    private bool keepBottom = true;
    private Sprite npcHead;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(keepBottom)
            scrollBar.value = 0;
    }

    public void InitHistoryMsg(string[] strList){
        for (int i = 0; i < strList.Length; i++) {
            GameObject obj = (GameObject)GameObject.Instantiate(textPrefab);
            obj.GetComponent<AdapatableText>().InputStr(strList[i]);
            obj.transform.SetParent(contentParant.transform);
        }
        scrollBar.gameObject.SetActive(false);
    }

    public bool isViewHistory() {
        return scrollBar.value > 0;
    }

    public void AddNewMsg(string data) {
        bool flag = isViewHistory();
        string addName = string.Format(data, StoryMgr.GetInstance().UserName);
        GameObject obj = (GameObject)GameObject.Instantiate(textPrefab);
        obj.GetComponent<AdapatableText>().InputStr(addName);
        obj.transform.SetParent(contentParant.transform);
        if (flag)
        {
            //todo
        }
        else {
            scrollBar.value = 0;
        }
    }

    internal void InitSendHeadImg(Sprite headImg)
    {
        npcHead = headImg;
    }

    internal void AddChooseMsg(string anniu1, string anniu2)
    {
        AddNewMsg(string.Format("choose Msg: 1,{0};2,{1}", anniu1, anniu2));
        //todo;
    }

    internal void AddTextMsg(string str, string time, bool isNewMsg)
    {
        if (time != null)
            AddNewMsg("time:" + time);
        AddNewMsg(str);
        //todo;
    }

    internal void AddImgagMsg(string image, string time, bool isNewMsg)
    {
        if(time != null)
            AddNewMsg("time:" + time);
        AddNewMsg("image:" + image);
        //todo;
    }

    internal void AddSelfMsg(string str)
    {
        AddNewMsg("self:" + str);
        //todo;
    }

    internal void AddWaitingFlag()
    {
        //todo
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        keepBottom = scrollBar.value <= 0;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        keepBottom = false;
    }
}
